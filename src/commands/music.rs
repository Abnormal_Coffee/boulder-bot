use poise::serenity_prelude::{Color, CreateEmbed, MessageBuilder};

use crate::{data::music_scraper::scrape_music, Context, Error};

#[poise::command(slash_command, prefix_command)]
pub async fn check_music(ctx: Context<'_>) -> Result<(), Error> {
	let music = ctx.data().music.lock().await;

	if music.is_ok() {
		let music = music.as_ref().unwrap();
		// I hate this code but it's the only way I could get it working

		let mut embed = CreateEmbed::default();
		embed
			.fields([
				("Album", music.get_album().await, true),
				("Track", music.get_song().await, true),
			])
			.thumbnail(music.get_image_url().await)
			.color(
				u32::from_str_radix(&music.get_colour().await.replace('#', ""), 16)
					.unwrap_or_else(|_| 16777216),
			);
		// .color(u32::from_str_radix(music.get_colour().await, 16).unwrap());

		let reply = MessageBuilder::new().build();

		ctx.send(|message| {
			message.reply(true).embed(|e| {
				*e = embed.clone();
				e
			})
		})
		.await?;

		ctx.reply(reply).await?; // This errors - something about an empty message, so the Result<> is discarded
	} else {
		ctx.reply("There was an issue running this command").await?;
	};

	Ok(())
}
