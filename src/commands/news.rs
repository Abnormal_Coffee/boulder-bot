use poise::serenity_prelude::ChannelId;

use crate::{Context, Data, Error};

#[poise::command(slash_command, required_permissions = "ADMINISTRATOR")]
pub async fn enable_boulder_news(ctx: Context<'_>) -> Result<(), Error> {
	let channel = ctx.channel_id();
	let reply = ctx.data().subscribe_channel_to_news(&channel).await;
	ctx.reply(reply).await?;
	Ok(())
}

#[poise::command(slash_command, required_permissions = "ADMINISTRATOR")]
pub async fn disable_boulder_news(ctx: Context<'_>) -> Result<(), Error> {
	let channel = ctx.channel_id();
	let response = ctx.data().unsubscribe_channel_to_news(&channel).await;
	ctx.say(response).await?;
	Ok(())
}

impl Data {
	async fn subscribe_channel_to_news(&self, channel: &ChannelId) -> String {
		let news_channels = &self.news_channels;
		if news_channels.lock().await.contains(channel) {
			return "This channel has already subscribed to news.".into();
		};
		news_channels.lock().await.push(*channel);
		"This channel has been subscribed to news".into()
	}

	async fn unsubscribe_channel_to_news(&self, channel: &ChannelId) -> String {
		let news_channels = &self.news_channels;
		let id_as_u64 = *channel.as_u64();
		let pos: Vec<usize> = news_channels
			.lock()
			.await
			.iter()
			.enumerate()
			.filter(|(_i, pos)| *pos.as_u64() == id_as_u64)
			.map(|(i, _)| i)
			.collect();
		for index in pos {
			news_channels.lock().await.swap_remove(index);
			return "Channel is no longer subscribed to news".into();
		}
		return "Channel isn't subscribed to news".into();
	}
}
