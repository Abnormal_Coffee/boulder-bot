use crate::{Context, Error};

#[poise::command(slash_command, prefix_command)]
pub async fn check_capacity(ctx: Context<'_>) -> Result<(), Error> {
	let reply = ctx.data().capacity_message().await;
	ctx.reply(reply).await?;
	Ok(())
}
