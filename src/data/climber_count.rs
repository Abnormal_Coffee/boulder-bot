static CLIMBER_COUNT_URL: &str =
	"https://portal.rockgympro.com/portal/public/620b59568a6c93407373bda88564f747/occupancy";

#[derive(serde::Serialize, serde::Deserialize)]
#[allow(non_snake_case)]
struct AAA {
	AAA: OccupancyData,
}

#[derive(serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
struct OccupancyData {
	capacity: u64,
	count: u64,
	sub_label: String,
	last_update: String,
}

pub async fn scrape_climbers(client: &reqwest::Client) -> Option<(u64, u64)> {
	let page = client.get(CLIMBER_COUNT_URL).send().await;

	match page {
		Ok(page) => {
			let body = page.text().await.unwrap();
			let re = regex::Regex::new("(?m)(?s)var data = (.+?);").unwrap();
			let captures = re.captures(&body).unwrap();
			let json_data = captures.get(1).unwrap().as_str().replace("'", "\"");
			// println!("{json_data}");
			let data: AAA = serde_jsonrc::from_str(&json_data).unwrap();
			Some((data.AAA.count, data.AAA.capacity))
		}
		Err(_) => None,
	}
}
