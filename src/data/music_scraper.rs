use anyhow::Result;
use reqwest::Client;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Music {
	data: Data,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Data {
	now_playing: NowPlaying,
}
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct NowPlaying {
	sound_zone: String,
	started_at: String,
	track: Track,
}
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Track {
	id: String,
	name: String,
	artists: Vec<Artist>,
	album: Album,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Artist {
	name: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Album {
	title: String,
	display: Display,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Display {
	colors: Colors,
	image: Image,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Colors {
	primary: Primary,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Primary {
	hex: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Image {
	size: String,
}

const SOUNDTRACK_URL: &str = "https://api.soundtrackyourbrand.com/v2";
const BODY: &str = r#"{"query":"query NowPlayingInitial($zone: ID!) {\nnowPlaying(soundZone: $zone) {\nsoundZone\n...NowPlayingData\n}\n}\nfragment NowPlayingData on NowPlaying {\nstartedAt\ntrack(market:US) {\nid\nname\nartists {\nname\n}\nalbum {\ntitle\ndisplay {\ncolors {\nprimary {\nhex\n}\n}\nimage {\nsize(width:1600, height:1600)\n}\n}\n}\n}\n}\n","variables":{"zone":"BGMCNA"}}"#;

impl Music {
	pub async fn get_song(&self) -> &str {
		return &self.data.now_playing.track.name;
	}

	pub async fn get_album(&self) -> &str {
		return &self.data.now_playing.track.album.title;
	}

	pub async fn get_image_url(&self) -> &str {
		return &self.data.now_playing.track.album.display.image.size;
	}

	pub async fn get_colour(&self) -> &str {
		return &self.data.now_playing.track.album.display.colors.primary.hex;
	}
}

pub async fn scrape_music(client: &Client) -> Result<Music> {
	let response = client
		.post(SOUNDTRACK_URL)
		.header("Content-Type", "application/json")
		.body(BODY)
		.send()
		.await?;

	let music: Music = response.json().await?;
	Ok(music)
}
