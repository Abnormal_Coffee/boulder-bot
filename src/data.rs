use crate::{updater::Update, Data};
use once_cell::sync::Lazy;
use tokio::sync::Mutex;

use self::{climber_count::scrape_climbers, music_scraper::scrape_music};
mod climber_count;
pub mod music_scraper;
pub use music_scraper::Music;

static REQWEST_CLIENT: Lazy<reqwest::Client> = Lazy::new(|| reqwest::Client::new()); // Global variables are discouraged, but this is read only, so it shouldn't matter.

impl Data {
	pub async fn generate(updater: Update) -> Self {
		Data {
			music: Mutex::new(scrape_music(&REQWEST_CLIENT).await),
			news_channels: Mutex::new(vec![]),
			capacity: Mutex::new(scrape_climbers(&REQWEST_CLIENT).await),
			update: Mutex::new(updater),
		}
	}
}

impl Data {
	pub async fn capacity_message(&self) -> String {
		let capacity = self.capacity.lock().await;
		if let Some(capacity) = *capacity {
			let (current, max) = capacity;
			return format!("Boulder Brighton is currently at {current}/{max}");
		};
		return "Sorry, there was an issue retreiving the data.".into();
	}
}

impl Data {
	pub async fn update_climber_count(&self) {
		let capacity = &self.capacity;
		*capacity.lock().await = scrape_climbers(&REQWEST_CLIENT).await;
	}

	pub async fn update_music(&self) {
		let music = &self.music;
		*music.lock().await = scrape_music(&REQWEST_CLIENT).await;
	}
}
