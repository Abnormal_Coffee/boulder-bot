use std::time::Instant;

use crossbeam_channel::Receiver;
use poise::serenity_prelude::Activity;

use crate::Context;

pub struct Update {
	pub music: Receiver<Instant>,
	pub climber_count: Receiver<Instant>,
}

pub async fn update_check(ctx: Context<'_>) {
	if let Ok(update) = &mut ctx.data().update.try_lock() {
		if let Ok(_) = update.music.try_recv() {
			ctx.data().update_music().await;
		};
		if let Ok(_) = update.climber_count.try_recv() {
			ctx.data().update_climber_count().await;
			ctx.serenity_context()
				.set_activity(Activity::watching(format!(
					"{:?} climbers at BB",
					ctx.data().capacity.lock().await.unwrap_or_else(|| { (0, 0) }).0
				)))
				.await;
		};
	}
}
