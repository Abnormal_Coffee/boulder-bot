use std::time::Duration;

use anyhow::{self, Result};
use commands::*;
use data::Music;
use dotenv::dotenv;
use error_handling::on_error;
use poise::{
	serenity_prelude::{self as serenity, ChannelId},
	FrameworkOptions,
};
use tokio::sync::Mutex;
use updater::Update;
mod commands;
mod data;
mod error_handling;
mod updater;

struct Data {
	music: Mutex<Result<Music>>,
	news_channels: Mutex<Vec<ChannelId>>,
	capacity: Mutex<Option<(u64, u64)>>,
	update: Mutex<Update>,
}

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

const SECONDS_IN_MINUTE: u64 = 60;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	// The following variables are tickers, they send a message down a channel every <Duration>, this is used for the updates.
	let (music, climber_count, climber_count_for_graph) = (
		crossbeam_channel::tick(Duration::from_secs(SECONDS_IN_MINUTE)),
		crossbeam_channel::tick(Duration::from_secs(5 * SECONDS_IN_MINUTE)),
		crossbeam_channel::tick(Duration::from_secs(20 * SECONDS_IN_MINUTE)),
	);

	let framework_options = FrameworkOptions {
		commands: vec![
			enable_boulder_news(),
			disable_boulder_news(),
			check_capacity(),
			check_music(),
		],
		pre_command: |ctx| Box::pin(updater::update_check(ctx)),
		on_error: |error| Box::pin(on_error(error)),
		..Default::default()
	};

	dotenv().expect("Couldn't find .env file");
	let token =
		dotenv::var("DISCORD_BOT_TOKEN").expect("Couldn't find DISCORD_BOT_TOKEN in .env file");

	let framework = poise::Framework::builder()
		.options(framework_options)
		.token(token)
		.intents(serenity::GatewayIntents::non_privileged())
		.setup(|ctx, ready, framework| {
			Box::pin(async move {
				let server_count = ready.guilds.iter().count();
				println!("Bot is ready; running on {server_count} server(s).");

				poise::builtins::register_globally(ctx, &framework.options().commands).await?;
				Ok(Data::generate(Update { music: music, climber_count: climber_count }).await)
			})
		});

	framework.run().await?;

	Ok(())
}
